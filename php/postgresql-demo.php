<?php
$conn = pg_connect('host=localhost port=5432 dbname=bdd user=bdd password=bdd');
if (!$conn) {
	echo 'Database connection failed!';
	exit(1);
}
$result = pg_query($conn, 'SELECT * FROM turista');
if (!$result) {
	echo 'SELECT failed: '.pg_last_error($conn);
	exit(1);
}

echo '<table border=1><tr>';
for ($i = 0; $i < pg_num_fields($result); $i++) {
	echo '<td>'.pg_field_name($result, $i).'</td>';
}
echo '</tr>';
while ($row = pg_fetch_row($result)) {
	echo '<tr>';
	for ($i = 0; $i < pg_num_fields($result); $i++) {
		echo '<td>'.$row[$i].'</td>';
	}
	echo '</tr>';
}
pg_free_result($result);
echo '</table>';
pg_close($conn);
?>
