namespace PostgresqlDemo {
	using Postgres;

	public static int main (string[] args) {
		Database conn = set_db_login ("localhost", "5432", "", "", "database", "user", "password");
		if (conn.get_status () != ConnectionStatus.OK) {
			stderr.printf ("%s\n", conn.get_error_message ());
			return 1;
		}

		Result res = conn.exec ("SELECT * FROM turista");
		if (res.get_status () != ExecStatus.TUPLES_OK) {
			stderr.printf ("SELECT failed: %s", conn.get_error_message ());
			return 1;
		}

		for (int i = 0; i < res.get_n_fields (); i++) {
			stdout.printf ("%-20s", res.get_field_name (i));
		}
		stdout.printf ("\n\n");

		for (int i = 0; i < res.get_n_tuples (); i++) {
			for (int j = 0; j < res.get_n_fields (); j++) {
				stdout.printf ("%-20s", res.get_value (i, j));
			}
			stdout.printf ("\n");
		}
		return 0;
	}
}
